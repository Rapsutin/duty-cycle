package juho.lamminmaki.gps;

/**
 * Created by juho on 7.11.2016.
 */

public class Average {

    private int samples;
    private float average;

    public Average() {
        samples = 0;
        average = 0;
    }

    public void addSample(float sample) {
        if (sample == 0) {return;}
        average = (average * samples + sample)/(samples+1);
        samples++;
    }

    public float getAverage() {
        return average;
    }

    public int getSampleCount() {
        return samples;
    }
}
