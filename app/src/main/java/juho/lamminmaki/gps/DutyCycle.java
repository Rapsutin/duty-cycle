package juho.lamminmaki.gps;

import android.util.Log;

/**
 * Created by juho on 7.11.2016.
 */

public class DutyCycle implements Runnable {

    private MainActivity mainActivity;
    private Average avgSpeed;
    private float maxError;

    public DutyCycle(MainActivity mainActivity, float maxError) {
        super();
        this.mainActivity = mainActivity;
        this.avgSpeed = new Average();
        this.maxError = maxError;
    }

    @Override
    public void run() {
        while(true) {
            mainActivity.updateLocation();
            Log.i("waiting time", String.valueOf(updateInterval()));
            try {
                Thread.sleep(updateInterval());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void recordSpeed(float speed) {
        avgSpeed.addSample(speed);
    }

    public float getSpeed() {
        return avgSpeed.getAverage();
    }

    public long updateInterval() {
        if(avgSpeed.getSampleCount() < 10) {return 1000;}
        return (long) (maxError/avgSpeed.getAverage() * 1000);
    }
}
